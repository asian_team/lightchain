from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
import Crypto.Random
import binascii

from app import api
from pymongo import errors

class Wallet:
    def __init__(self, node_id):
        self.private_key = None
        self.public_key = None
        self.node_id = node_id

    async def create_keys(self):
        private_key, public_key = self.generate_keys()
        self.private_key = private_key
        self.public_key = public_key

    async def save_keys(self):
        try:
            result = await api.mongodb['wallets'].find_one({"port": {"$eq": self.node_id} })
            if result is None:
                try:
                    if self.public_key is not None and self.private_key is not None:
                        info_wallet = {
                            "port": self.node_id,
                            "public_key": self.public_key,
                            "private_key": self.private_key
                        }
                        await api.mongodb['wallets'].insert_one(info_wallet)
                        return True
                except errors.WriteError as e:
                    if e.details.get("code") != 11000:
                        print(e.details.get('errmsg'))
                        return False
                    else:
                        return True
            else:
                raise ValueError("Wallet already exist")
        except ValueError as e:
            print(e)
            return False

    async def load_keys(self):
        try:
            result = await api.mongodb['wallets'].find_one({"port": {"$eq": self.node_id} })
            if result is None:
                raise ValueError("No wallet found")
            public_key = result['public_key']
            private_key = result['private_key']
            self.public_key = public_key
            self.private_key = private_key
            return True
        except ValueError as e:
            print(e)
            return False

    def generate_keys(self):
        private_key = RSA.generate(1024, Crypto.Random.new().read)
        public_key = private_key.publickey()
        return binascii.hexlify(private_key.exportKey(format='DER')).decode('ascii'), binascii.hexlify(
            public_key.exportKey(format='DER')).decode('ascii')

    def sign_transaction(self, sender, recipient, amount):
        signer = PKCS1_v1_5.new(RSA.importKey(binascii.unhexlify(self.private_key)))
        h = SHA256.new((str(sender) + str(recipient) + str(amount)).encode('utf-8'))
        signature = signer.sign(h)
        return binascii.hexlify(signature).decode('ascii')

    @staticmethod
    def verify_transaction(transaction):
        if transaction.sender == 'MINING':
            return True
        public_key = RSA.importKey(binascii.unhexlify(transaction.sender))
        verifier = PKCS1_v1_5.new(public_key)
        h = SHA256.new((str(transaction.sender) + str(transaction.recipient) + str(transaction.amount)).encode('utf-8'))
        return verifier.verify(h, binascii.unhexlify(transaction.signature))
